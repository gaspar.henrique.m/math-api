
from fatorial_controller import FatorialController
import falcon

class FatorialResource:
    
    def on_get(self, req, resp):
        header = req.get_header('Authorization')
        if header is None or header != "sel0456":
            raise falcon.HTTPUnauthorized(
                title='Invalid header',
                description="Provides a valid header!",
            )       
        number = req.params.get('number', None)
        number = int(number)
        fatorial_controller = FatorialController(req.context)
        fatorial_result = fatorial_controller.get_fatorial_number(number)
        resp.media = {
            "fatorial_result": fatorial_result
        }
        resp.status = 201  