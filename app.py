
import falcon
from wsgiref.simple_server import make_server
from fatorial_resource import FatorialResource



fatorial = FatorialResource()

app = falcon.App()

app.add_route('/fatorial', fatorial)

if __name__ == '__main__':
    with make_server('', 8000, app) as httpd:
        print('Serving on port 8000...')

        # Serve until process is killed
        httpd.serve_forever()
